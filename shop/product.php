<?php include 'config.php';
if (isset($_GET['id'])) {
    $id = intval($_GET['id']);
} else {
    $id = 1;
}
$product = Entity\Product::find($id);
$reviews = $product->getReviews();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <title>WEB-04 ONLINE-SHOP</title>
    <link rel="stylesheet" type="text/css" href="stile.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script src="https://kit.fontawesome.com/ade0e905b9.js" crossorigin="anonymous"></script>
</head>
<body>
<?php include 'topbar.php'; ?>
<div id="content" class="container ">
    <div id="menu" class="col-12 row menu ">
        <?php include 'header.php'; ?>
    </div>


    <div id="body">

        <div class=" row" style="margin: 0px;background: red">
            <div id="sidebar" class="col-2 localContent">
                <?php include 'sidebar.php'; ?>
            </div>

            <div id="fullcolor" class="col-7">

                <center><h2><?php echo $product->title; ?></h2></center>
                <div class="row justify-content-center align-items-center">
                    <div><img class="shadow p-3 mb-5 bg-white rounded" src="img/<?php echo $product->photo; ?>" width="400" height="400"></div>
                    <div>
                        <h5 style="font-size: medium;color: brown;"><button class="btn btn-primary"  type="button" disabled>
                               Price: <span class="badge">
                            <?php echo '' .$product->price. ' RON'; ?></span></button></h5>
                        <?php if ($product->discount>0):?>
                            <h6 style="color: brown;font-weight: 800"><button class="btn btn-danger" type="button" disabled><span class="badge"><?php echo '-'.$product->discount.'%' ;?></span></button></h6>
                        <?php endif;?>
                        <h6 style="color: black; padding:10px">Product details: <?php echo $product->description; ?>
                        </h6>
                    </div>

                    <a href="?code=<?php echo $id;?>" class="btn btn-warning">Add to Basket</a>
                </div>

                <?php 
				

						 if (!empty($product)) {
						 foreach ($reviews as $review) {  


                            ?>
                    <div class="row justify-content-center align-items-center">
                        <div class="col-12 reviewer"><?php echo $review->user.' : '.$review->description;?></div>
                        <div class="col-12 reviewRate"><?php echo 'Rated this product : '.$review->nota;?></div>
                    </div>
                    <br>
               <?php
						 }  } else { echo "
						<div class='row justify-content-center align-items-center'> 
						<p>No reviews yet for this product.</p>
						</div>
						"; }

 
                   
$username = Entity\User::find($_SESSION['username']);
$user = $username->getUsername();
$email = $username->getEmail();
                        ?>
                        <br>
                        <div>
                            <form action="processReviews.php" method="post">
                                <h6>Please rate this product</h6>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="nota" id="inlineRadio1"
                                           value="1">
                                    <label class="form-check-label" for="inlineRadio1">1</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="nota" id="inlineRadio2"
                                           value="2">
                                    <label class="form-check-label" for="inlineRadio2">2</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="nota" id="inlineRadio3"
                                           value="3">
                                    <label class="form-check-label" for="inlineRadio3">3</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="nota" id="inlineRadio4"
                                           value="4">
                                    <label class="form-check-label" for="inlineRadio4">4</label>
                                </div>
                                <input type="hidden" name="email" value=<?php echo $email; ?>>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="nota" id="inlineRadio5"
                                           value="5">
                                    <label class="form-check-label" for="inlineRadio5">5</label>
                                </div>
                                <input type="hidden" name="prod_id" value=<?php echo $id; ?>>

                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Write your description here!</label>
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
                                              name="description"></textarea>
                                </div>
                                <input type="hidden" name="user" value=<?php echo $user; ?>>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                 

                <br>
                <div>
                    <h5>
                        <center>Similar Products</center>
                    </h5>
                </div>
                <div class="row justify-content-center align-items-center">
                   <?php

                    $similarProducts = $product->getSimilarProducts();
                    //Similar products are products from the same cathegory as the main product

                   for ($i=0; $i<3; $i++){ $similarProduct=$similarProducts[$i];
                        $card_title = substr($similarProduct->title, 0, 20);
                        $card_text = substr($similarProduct->description, 0, 40); ?>
                        <div class="card" style="margin:5px 5px; width: 200px;height: 450px;">
                            <img src="img/<?php echo $similarProduct->photo; ?>" class="card-img-top" style="height:155px">
                            <div class="card-body">
                                <h4 class="card-title"><?php echo $card_title; ?></h4>
                                <p class="card-text"><?php echo $card_text; ?></p>
                                <a href="product.php?id=<?php echo $similarProduct->id; ?>"
                                   class="btn btn-outline-success">View</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div id="bestseller" class="col-3 ">
                <?php include 'bestseller.php'; ?>
            </div>
        </div>
    </div>
    <div id="footer"><?php include 'footer.php'; ?></div>
</div>
</body>
</html>