<?php
$allcategories = Entity\Category::findBy();

foreach ($allcategories as $category) {
    $products = $category->getProductsForIndex();
                                                     ?>
<div class="row justify-content-center align-items-center">
        	<?php foreach ($products as $product):
    $card_title = substr($product->title, 0, 20);
    $card_text = substr($product->description, 0, 30);

    echo "<div class='card' style='margin: 5px 5px; width: 200px;height: 450px'>
														<img src='img/$product->photo' class='card-img-top' style='height:155px'>
														<div class='card-body'>
														<h5 class='card-title' style='color:blue'><a href='product.php?id=$product->id'>$card_title</a></h5>
														<p class='card-text'>$card_text...</p>
														<a href='product.php?id=$product->id' class='btn btn-outline-info'><i class='far fa-eye'> View</i></a>
														<a href='?action=wishlist&code=$product->id' class='btn btn-outline-danger'><i class='far fa-heart'></i></a>
														<a class='btn btn-warning' href='?code=$product->id' style='margin: 5px 5px; color: white;'><i class='fas fa-cart-plus'> Buy</i></a>
														</div>
													</div>";

endforeach; ?>
</div>
<br>
 <?php
}
?>
