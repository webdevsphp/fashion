<?php
namespace Entity;

class Orders extends Base
{
    public $user_id;

    /**
     * @return mixed
     */
    public static function tableName(){
        return 'orders';
    }
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Orders
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     * @return Orders
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * @param mixed $product_id
     * @return Orders
     */
    public function setProductId($product_id)
    {
        $this->product_id = $product_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return Orders
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return mixed
     */

    /**
     * @return mixed
     */


    public $product_id;

    public $quantity;


    public function getOrders()
    {
        $data = query("SELECT * FROM orders ORDER by id DESC");
        $list = [];

        foreach ($data as $dbLine){
            $list[] = new Orders ($dbLine['id']);
        }

        return $list;
    }

}