
<div class="row justify-content-center align-items-center"> <h2>Best Sellers</h2> </div>
<?php
$products=Entity\Product::findBy(['featured'=>'1'],'id','DESC','4');
foreach ($products as $product):
?>
		 <div "row justify-content-center align-items-center"><div class="card" style="width: 12rem;">
  <img class="card-img-top" src="img/<?php echo $product->photo; ?>" alt="Card image cap">
  <div class="card-body">
    <h4 class="card-title"><?php echo $product->title; ?></h4>
    <p class="card-text"><?php echo substr($product->description, 0, 40); ?></p>
    <a href="product.php?id=<?php echo $product->id; ?>" class="btn btn-primary">View</a>
  </div>
</div></div>
<br>
<?php endforeach; ?>