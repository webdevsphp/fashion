<nav class="navbar navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">	
			<ul class="nav nav-tabs nav-pills  mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="../../index.php">Home </a>
          </li>
		 <li class="nav-item">
            <a class="nav-link" href="../../newest.php">New Arrivals</a>
          </li>
		  <li class="nav-item">
            <a class="nav-link" href="../../discounts.php">Discounts</a>
          </li>
		  <li class="nav-item">
            <a class="nav-link" href="#">Shipping & Payment</a>
          </li>
		  <?php    $user = Entity\User::find($_SESSION['username']);
                     $user = $user->getUsername();
                if ($user=="admin") { echo "<li class='nav-item'>
            <a class='nav-link' href='../admin/'>Admin</a>
          </li>"; } ?>
          <li class="nav-item">
            <a class="nav-link" href="../../contact.php">Contact</a>
          </li>
        </ul>   
  </nav>
  <nav class="d-flex ml-auto p-2 navbar">
			<form class="form-inline">
			<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
			</nav> 