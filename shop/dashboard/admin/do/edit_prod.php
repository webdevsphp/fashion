<?php

$id = intval($_GET['id']);
$product = Entity\Product::find($id);
$category = Entity\Category::findBy();
$category_id =$product->getCategoryId();

$gender=$product->getGender();
$genderln=strlen($product->getGender());
$size = str_replace(' ', '', $product->getSize());
$sizes =(explode(',', $size));


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Post Product</title>
</head>

<body>
<div class="wrapper">
  <div class="row">
	<div class="col-9">
    <td>
      
        
          <div class="row justify-content-center align-items-center"><h3>Edit Product</h3></div></td>
        
        <div class="row">
          <form id="form1" name="form1" method="post" action="do/do_product.php?edit=<?php echo $product->id;?>">
              <label>&nbsp;Title:
                <input name="title" type="text" size="100" value="<?php echo $product->title;?>" />
              </label> </div>
		<div class="row">
           <div class="col-3"><?php $this_category = Entity\Category::find($category_id);  ?>
             <p> <label> Category:
                     <select name="cat_id"><option value="<?php echo $category_id; ?>"><?php echo "($category_id) ".$this_category->getName();?></option>
                         <?php foreach ($category as $cat) { if ($category_id!=$cat->getId()): echo"<option value=$cat->id'>($cat->id) $cat->name</option>"; endif;} ?>
                     </select>
                 </label>

             </p>
           </div>
            <div class="form-check form-check-inline"><p>Gender:&nbsp;</p>
  <input class="form-check-input" name="gender1" type="checkbox" <?php if (($genderln==4) || ($genderln==5) || ($genderln==11)) { echo "checked";} ?> id="inlineCheckbox1" value="male">
  <label class="form-check-label" for="inlineCheckbox1">Male</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" name="gender2" <?php if (($genderln==6) ||($genderln==7) || ($genderln==11)) {echo"checked";} ?> type="checkbox" id="inlineCheckbox2"  value="female">
  <label class="form-check-label" for="inlineCheckbox2">Female</label>
</div>

<div class="form-check form-check-inline"><p>Sizes:&nbsp;</p>
  <input class="form-check-input" name="box1" type="checkbox" <?php foreach($sizes as $value) {
                                                                if ($value=='S') echo"checked"; } ?> id="inlineCheckbox3" value="S">
  <label class="form-check-label" for="inlineCheckbox3">S</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" name="box2" type="checkbox" <?php foreach($sizes as $value) { if ($value=='M') echo"checked"; } ?> id="inlineCheckbox4" value="M">
  <label class="form-check-label" for="inlineCheckbox4">M</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" name="box3" type="checkbox" <?php foreach($sizes as $value) { if ($value=='L') echo"checked"; } ?> id="inlineCheckbox5" value="L">
  <label class="form-check-label" for="inlineCheckbox5">L</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" name="box4" type="checkbox" <?php foreach($sizes as $value) { if ($value=='XL') echo"checked"; } ?> id="inlineCheckbox6" value="XL">
  <label class="form-check-label" for="inlineCheckbox6">XL</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" name="box5" type="checkbox" <?php foreach($sizes as $value) { if ($value=='XXL') echo"checked"; } ?> id="inlineCheckbox7" value="XXL">
  <label class="form-check-label" for="inlineCheckbox7">XXL</label>
</div> </div>
<div class="row">
		<div class="col-2">  
<div class="form-check form-check-inline">
<p> &nbsp;Featured:&nbsp;</p>

  <input class="form-check-input" name="featured" type="checkbox" <?php if ($product->featured==1) echo"checked"; ?> id="inlineCheckbox5" value="1">
  <label class="form-check-label" for="inlineCheckbox5"></label>
</div>
</div>
  <div class="col-2"> 

 &nbsp;Price:&nbsp;
	<label>
                <input name="price" type="text" size="6" value="<?php echo $product->price;?>" />&nbsp;LEI&nbsp;
              </label>
			  </div>
<div class="col-2"> 

 &nbsp;Discount:&nbsp;
	<label>
                <input name="discount" type="text" size="3" value="<?php echo $product->discount;?>" />&nbsp;%
              </label>
			  
			
		</div>
	
</div>
			   
<div class="row">
	<div class="col-12">
              <p><br>
                <label>
                <textarea name="content" cols="100" rows="15" ><?php echo $product->description;?></textarea>
                </label>
              </p>
              <p> <button type="submit" class="btn btn-success">UPDATE</button>
			  </p>
          </form>          </td>
     </div></div>
  
  </div>
  <div class="col-3 border border-warning"" style="float:right;">
  <?php 
		if (isset($_GET['session']) && (($_GET['session'])==0)) { echo "
	<div class='custom-file'> 
 <form method='post' action='do/upload_photo.php?edit=$id' enctype='multipart/form-data'>
  <div class='alert alert-danger' role='alert'>
  First upload an image!
</div><br /><br />
    <input type='file' name='file[]' id='file' multiple>
	 <input type='submit' name='submit' value='Upload'></form>
			</div>
	</div>
  ";  } elseif (isset($_GET['session']) && (($_GET['session'])==1)) { $product->setPhoto($_SESSION['photo']); 
		$image_path = "../../img/".$product->photo;
			echo "<a href='?id=$id&session=0'>Delete</a>
        <img src='$image_path' width='100%'/>
		"; } else { $image_path = "../../img/".$product->photo;
			echo "<a href='?id=$id&session=0'>Delete</a>
        <img src='$image_path' width='100%'/>
		"; } 

$_SESSION['photo']=$product->photo;
 
  ?>
</div>
</body>
</html>
