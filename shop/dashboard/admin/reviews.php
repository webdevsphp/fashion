<?php
include '../../config.php';
include '../isadmin.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <title>WEB-04 ONLINE-SHOP</title>
    <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/ade0e905b9.js" crossorigin="anonymous"></script>
</head>
<body>
<?php include 'topbar.php';?>
<div id="content" class="container "  >

  <div id="menu" class="col-12 row menu ">
       <?php include 'admin_header.php'; ?>
	    </div>

<div id="body">


        <div id="fullcolor" class="col-12" > 

         <div class="bs-example p-3 w-100"> 
            <!-- Bootstrap Grid -->
            <div class="row">
                <div class="col-1">Product Id</div>
                <div class="col-1">Rating</div>
                <div class="col-3">Description</div>
                <div class="col-1">Username</div>
                <div class="col-2">Status</div>
                <div class="col-4">Actions</div>
            </div>
            <?php
            $allreviews = Entity\Review::findBy();
            foreach ($allreviews as $review)
            {

                ?>
                <div class="row p-3 w-100">
                    <div class="col-1"><?php echo $review->prod_id;?></div>
                    <div class="col-1"><?php echo $review->nota;?></div>
                    <div class="col-3"><?php echo $review->description;?></div>
                    <div class="col-1"><?php echo $review->user;?></div>
                    <div class="col-2"><?php echo $review->status;?></div>
                    <div class="col-4">
                        <div class="row">
                            <div class="col-12">
                                <a href="acceptReview.php?id=<?php echo $review->id; ?>">Accept</a><!-- MERGE  -->
                                <a href="editReview.php?id=<?php echo $review->id; ?>">Edit</a><!--  MERGE  -->
                                <a href="deleteReview.php?id=<?php echo $review->id; ?>">Delete</a> <!-- MERGE  -->
                            </div>

                        </div>

                    </div>
                </div>
                <br>
                <?php
            }
            ?>
        </div>
       </div> 
    </div>
</div>
</body>
</html>