<nav class="navbar navbar-expand-lg navbar-light">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">	
			<ul class="nav nav-tabs nav-pills  mr-auto">
          		  <li class="nav-item active" id="admin_header">
            <a class="nav-link" href="../account/">My Account </a>
          </li>
		 <li class="nav-item">
            <div class="dropdown">
  <button type="button"  class="nav-link" data-toggle="dropdown">
    PRODUCTS
  </button>
  <div class="dropdown-menu" >
    <a class="dropdown-item" href="index.php">VIEW ALL</a>
    <a class="dropdown-item" href="add_product.php">ADD</a>
  </div>
</div>
          </li>
		  <li class="nav-item">
             <div class="dropdown">
  <button type="button" class="nav-link" data-toggle="dropdown">
    CATEGORY
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="category.php">VIEW ALL</a>
    <a class="dropdown-item" href="addCategory.php">ADD</a>
  </div>
</div>
          </li>
		  <li class="nav-item">
                       <div class="dropdown">
  <button type="button" class="nav-link" data-toggle="dropdown">
    REVIEWS
  </button>
  <div class="dropdown-menu">
    <a class="dropdown-item" href="reviews.php">VIEW ALL</a>
    
  </div>
</div>
         
          </li>
		  <li class="nav-item">
                       <div class="dropdown">
  <button type="button" class="nav-link" data-toggle="dropdown">
    ORDERS
  </button>
  <div class="dropdown-menu">
      <a class="dropdown-item" href="orders.php">VIEW ORDERS</a>
      <a class="dropdown-item" href="wishlists.php">VIEW WISHLISTS</a>
      <a class="dropdown-item" href="baskets.php">VIEW BASKETS</a>

    
  </div>
</div>
         
          </li>
		   <li class="nav-item">

               <div class="dropdown">
                   <button type="button" class="nav-link" data-toggle="dropdown">
                       USERS
                   </button>
                   <div class="dropdown-menu">
                       <a class="dropdown-item" href="user.php">VIEW USERS</a>
                       <a class="dropdown-item" href="addUser.php">ADD USER</a>


                   </div>
               </div>

           </li>
                       <div class="dropdown">
  <button type="button" class="nav-link" data-toggle="dropdown">
    SETTINGS
  </button>
  <div class="dropdown-menu">
      <a class="dropdown-item" href="#">EDIT SHIPPING POLICY</a>
      <a class="dropdown-item" href="#">EDIT PAYMENT TYPE</a>
      <a class="dropdown-item" href="#">EDIT POLICY</a>
    
  </div>
</div>
         
          </li>
        </ul>   
  </nav>
  <nav class="d-flex ml-auto p-2 navbar">
			<form class="form-inline">
			<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
			<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
			</nav> 