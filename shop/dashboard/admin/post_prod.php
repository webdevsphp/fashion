<?php 
$product = Entity\Product::find(1);
$categories = $product->getCategories();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Post Product</title>
</head>

<body>
<div class="wrapper">
  <div class="row">
	<div class="col-9">
    <td><p>&nbsp;</p>
      
        
          <div class="row justify-content-center align-items-center"><h3>ADD Product</h3></div></td>
        
        <div class="row">
          <form id="form1" name="form1" method="post" action="upload.php">
              <label>&nbsp;Title:
                <input name="title" type="text" size="100" />
              </label> </div>
		<div class="row">
		<div class="col-3">
             <p>
                <label>&nbsp;Category:
                <select name="cat_id">
				
				<?php foreach ($categories as $category):?>
     <option value="<?php echo $category->getId()?>"><?php echo $category->getName();?></option>
<?php endforeach; ?>
  <option selected value="5">ADD TO CATEGORY</option>
</select>

                </label>
              </p>
			</div>
			
			<div class="form-check form-check-inline"><p>Gender:&nbsp;</p>
  <input class="form-check-input" name="gender1" type="checkbox" id="inlineCheckbox1" value="male">
  <label class="form-check-label" for="inlineCheckbox1">Male</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" name="gender2" type="checkbox" id="inlineCheckbox1" value="female">
  <label class="form-check-label" for="inlineCheckbox1">Female</label>
</div>

<div class="form-check form-check-inline"><p>Sizes:&nbsp;</p>
  <input class="form-check-input" name="box1" type="checkbox" id="inlineCheckbox1" value="S">
  <label class="form-check-label" for="inlineCheckbox1">S</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" name="box2" type="checkbox" id="inlineCheckbox2" value="M">
  <label class="form-check-label" for="inlineCheckbox2">M</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" name="box3" type="checkbox" id="inlineCheckbox3" value="L">
  <label class="form-check-label" for="inlineCheckbox3">L</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" name="box4" type="checkbox" id="inlineCheckbox4" value="XL">
  <label class="form-check-label" for="inlineCheckbox4">XL</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" name="box5" type="checkbox" id="inlineCheckbox5" value="XXL">
  <label class="form-check-label" for="inlineCheckbox5">XXL</label>
</div> </div>
<div class="row">
		<div class="col-2">  
<div class="form-check form-check-inline">
<p> &nbsp;Featured:&nbsp;</p>
  <input class="form-check-input" name="featured" type="checkbox" id="inlineCheckbox5" value="1">
  <label class="form-check-label" for="inlineCheckbox5"></label>
</div>
</div>
  <div class="col-2"> 

 &nbsp;Price:&nbsp;
	<label>
                <input name="price" type="text" size="6" />&nbsp;LEI&nbsp;
              </label>
			  </div>
<div class="col-2"> 

 &nbsp;Discount:&nbsp;
	<label>
                <input name="discount" type="text" size="3" />&nbsp;%
              </label>
			  
			
		</div>
	
</div>
			   
<div class="row">
	<div class="col-12">
              <p><br>
                <label>
                <textarea name="content" cols="100" rows="15"></textarea>
                </label>
              </p>
              <p> <button type="submit" class="btn btn-success">SUBMIT</button>
			  </p>
          </form>          </td>
     </div></div>
  
  </div>
  <div class="col-3 border border-warning"" style="float:right;">
  <?php 
  if ((isset($_GET['session'])) && ($_GET['session']==1)) { 
		$image_path = "../../img/".$_SESSION['photo']; 
			echo "<a href='?session=0'>Delete</a>
        <img src='$image_path' width='100%'/>
       ";
  
  
  } else { echo  "
	<div class='custom-file'> 
 <form method='post' action='do/upload_photo.php' enctype='multipart/form-data'>
  <div class='alert alert-danger' role='alert'>
  First upload an image!
</div><br /><br />
    <input type='file' name='file[]' id='file' multiple>
	 <input type='submit' name='submit' value='Upload'></form>
			</div>
	</div>
  ";} 
 
  ?>
</div>
</body>
</html>
