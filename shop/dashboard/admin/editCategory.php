<?php
include "../../config.php";

$category = Entity\Category::find(intval($_GET['id']));
//$nr = $categories[0]["count(*)"];
$_SESSION['cat_id']=intval($_GET['id']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <title>WEB-04 ONLINE-SHOP</title>
    <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ade0e905b9.js" crossorigin="anonymous"></script>
</head>
<body>
<?php include 'topbar.php';?>
<div id="content" class="container "  >

    <div id="menu" class="col-12 row menu ">
        <?php include 'admin_header.php'; ?>
    </div>

    <div id="body">


        <div id="fullcolor" class="col-12" >

            <div class="bs-example p-3 w-100">
                <form action="processEditCategory.php?id=<?php echo intval($_GET['id']); ?>" method="post">
        <div class="form-row">
            <label for="cathegory">Category name</label>
                        <input type="text" class="form-control" name="name" placeholder="Category" value="<?php echo $category->name;?>"/>
                    </div>
                    <div class="form-row">
                        <label for="id">Id</label>
                        <input class="form-control" type="text" title="id" value="<?php echo $category->id;?>" readonly>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>