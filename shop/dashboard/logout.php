<?php
include "functions.php";
isAuthenticated();
session_destroy();
header('Location: ../index.php');
die;