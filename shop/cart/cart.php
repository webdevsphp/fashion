<?php

$status="";

if (isset($_POST['action']) && $_POST['action']=="change"){
  foreach($_SESSION["shopping_cart"] as &$value){
    if($value['code'] === $_POST["code"]){
        $value['quantity'] = $_POST["quantity"];
        break; // Stop the loop after we've found the product
    }
}
  	
}


?>
<html>
<head>
<title>Shopping Cart</title>
<link rel='stylesheet' href='../css/style.css' type='text/css' media='all' />
</head>
<body>
<div style="width:700px; margin:50 auto;">

<h2>Shopping Cart</h2>   

<?php
if(!empty($_SESSION["shopping_cart"])) {
$cart_count = count(array_keys($_SESSION["shopping_cart"]));
?>
<div class="cart_div">
<a href="cart.php">
<img src="cart/cart-icon.png" /> Cart
<span class="badge badge-primary"><?php echo $cart_count; ?></span></a>
</div>
<?php
}
?>

<div class="cart">
<?php
if(isset($_SESSION["shopping_cart"])){
    $total_price = 0;
?>	
<table class="table">
<tbody>
<tr>
<td></td>
<td>ITEM NAME</td>
<td>QUANTITY</td>
<td>UNIT PRICE</td>
<td>ITEMS TOTAL</td>
</tr>	
<?php		
foreach ($_SESSION["shopping_cart"] as $product){
?>
<tr>
<td><img src='../../img/<?php echo $product["image"]; ?>' width="50" height="40" /></td>
    <td><a href="../../product.php?id=<?php echo $product["code"]; ?>"><?php echo $product["name"]; ?></a><br />
<!-- <form method='post' action=''> -->
<input type='hidden' name='code' value="<?php echo $product["code"]; ?>" />
<input type='hidden' name='action' value="remove" />
<a class="btn btn-outline-danger" href="?action=remove&code=<?php echo $product["code"]; ?>">Remove Item</a>
</form>
</td>
<td>
<form method='post' action=''>
<input type='hidden' name='code' value="<?php echo $product["code"]; ?>" />
<input type='hidden' name='action' value="change" />
<select name='quantity' class='quantity' onchange="this.form.submit()">
<option <?php if($product["quantity"]==1) echo "selected";?> value="1">1</option>
<option <?php if($product["quantity"]==2) echo "selected";?> value="2">2</option>
<option <?php if($product["quantity"]==3) echo "selected";?> value="3">3</option>
<option <?php if($product["quantity"]==4) echo "selected";?> value="4">4</option>
<option <?php if($product["quantity"]==5) echo "selected";?> value="5">5</option>
</select>
</form>
</td>
<td><?php echo "LEI ".$product["price"]; ?></td>
<td><?php echo "LEI ".$product["price"]*$product["quantity"]; ?></td>
</tr>
<?php
$total_price += ($product["price"]*$product["quantity"]);
}
$_SESSION['totalprice']= $total_price;
?>
<tr>
<td colspan="2" align="left"> <a class="btn btn-outline-danger" href='?action=removeall'>Remove ALL</a></td>
<td colspan="5" align="right">
<strong>TOTAL: <?php echo "LEI ".$total_price; ?></strong>
</td>

</tr>
<tr>

<td colspan="5" align="right"><a class="btn btn-success" href="checkout.php">
Checkout</a>
</td>

</tr>

</tbody>
</table>		
  <?php
}else{ if (isset($_GET['action']) && ($_GET['action']=="ordercompleted")) {  echo "<h3>Your order is completed!</h3><BR><h4>Thank you!</h4>"; } 
			else { echo "<h3>Your cart is empty!</h3>"; }
	}
?>
</div>

<div style="clear:both;"></div>

<div class="message_box" style="margin:10px 0px;">
<?php echo $status; ?>
</div>


<br /><br />
 <br /><br />

</div>
</body>
</html>