<?php


    $message=100;
    /* messages:
     * 0 - delete all products
     * 1 - success - product added to basket
     * 2 - remove product from basket
     * 3 - login first

    */
    $message_success='<div class="alert alert-primary" role="alert">
                            Product was added to your basket!<a href="basket.php" class="alert-link"> Check your shopping basket</a>.
                                </div>';

    $message_remove='<div class="alert alert-warning" role="alert">
                            Product was removed from your basket!<a href="basket.php" class="alert-link"> View basket</a>.
                                </div>';

    $message_deleteall='<div class="alert alert-danger" role="alert">
                            Your shopping basket is now empty!
                                </div>';

    $message_loginfirst='<div class="alert alert-warning" role="alert">
                            You need to login first!<a href="dashboard/login.php" class="alert-link"> Login</a>.
                                </div>';

    if (isset($_SESSION['username'])) {
        /** @var User $username */
        $username = Entity\User::find($_SESSION['username']);

        $user = $username->getUsername();
    }

    if (isset($_GET['action']) && $_GET['action'] == 'removeall') {
        unset($_SESSION["shopping_cart"]);
        $message = 0;
    }

    if (isset($_GET['action']) && $_GET['action'] == "wishlist") {

        if (!isset($_SESSION['username'])) { $message= 3;} else {
            $username = Entity\User::find($_SESSION['username']);
            $id = $_GET['code'];
            $wishid = $username->getWishlistId();
            if ($wishid == 0) {
                $tobeadded = ['user_id' => $username->getId(), 'product_id' => $id];
                $new_wish = Entity\Wishlist::fromArray($tobeadded); $new_wish->save();
                echo '<script type="text/javascript">
		alert("Product was added to your wish list!")
		</script>';
            } else {
                $wish=[];
                $wish[] = $id;
                $addtowish = Entity\Wishlist::find($username->getWishlistId());

                if ($addtowish->getProductId()=="") {$addtowish->setProductId($id); $addtowish->save(); echo '<script type="text/javascript">
		alert("Product was added to your wish list!")
		</script>'; } else {
                    $wish[] = $addtowish->getProductId();
                    $product_id = implode(' ,', $wish);
                    $addtowish->setProductId($product_id);
                    $addtowish->save();
                    echo '<script type="text/javascript">
		alert("Product was added to your wish list!")
		</script>'; }
            }
        }


    }

    if (isset($_GET['action']) && $_GET['action'] == "remove") {

        if (!empty($_SESSION["shopping_cart"])) {

            foreach ($_SESSION["shopping_cart"] as $key => $value) {
                if ($_GET["code"] == $value['code']) {
                    unset($_SESSION["shopping_cart"][$key]);
                    $message = 2;

                }
                if (empty($_SESSION["shopping_cart"]))
                    unset($_SESSION["shopping_cart"]);
            }
        }
    }

    if ((isset($_GET['code'])) && (!isset($_GET['action']))) {

        $code = intval($_GET['code']);
        $result = Entity\Product::find($code);

        $name = $result->getTitle();
        $code = $result->getId();
        $price = $result->getFinalPrice();
        $image = $result->getPhoto();

        $cartArray = array(
            $code => array(
                'name' => $name,
                'code' => $code,
                'price' => $price,
                'quantity' => 1,
                'image' => $image)
        );

        if (empty($_SESSION["shopping_cart"])) {
            $_SESSION["shopping_cart"] = $cartArray;

        } else {
            $array_keys = array_keys($_SESSION["shopping_cart"]);
            if (in_array($code, $array_keys)) {
                echo '<script type="text/javascript">
		alert("This product is already in your cart!")
		</script>';
            } else {
                $_SESSION["shopping_cart"] = array_merge($_SESSION["shopping_cart"], $cartArray);
                $message = 1;

            }

        }
    }

    ?>
    <nav id="topbar" style="margin: 0 0; background: #a5d6a7; " class="navbar navbar navbar-light">
        <div class="container">

            <p>Tel: +40 123 456 - email@mail.com - Monday-Friday: 10am-06pm
            <p>
                <?php if (isset($user)) {
                    echo "$user
							<a class='navbar-brand btn btn-outline-warning' style='color:white' href='dashboard/logout.php'><b>Logout</B></a>";
                } else {
                    echo "<a class='navbar-brand btn btn-outline-info' style='color:white' href='dashboard/login.php'><b>Login</B></a>
						<a class='navbar-brand btn btn-outline-warning' style='color:white' href='dashboard/register.php'><b>Register</B></a>
						";
                } ?>

                <a class="navbar-brand btn btn-outline-success" href="basket.php">
                    <i class="fas fa-shopping-cart" style="color:white">&nbsp;<span class="badge badge-light">
  <?php
  if (!empty($_SESSION["shopping_cart"])) {
      $cart_count = count(array_keys($_SESSION["shopping_cart"]));
      echo "$cart_count";
  } else {
      echo "0";
  } ?></span></i></a>

        </div>
    </nav>
    <?php
    if ($message == 0) { echo $message_deleteall;}
    if ($message == 1) { echo $message_success;}
    if ($message == 2) { echo $message_remove;}
    if ($message == 3) { echo $message_loginfirst;}
?>