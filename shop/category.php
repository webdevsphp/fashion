<?php include 'config.php';

if (isset($_GET['id']))
{
    $id=intval($_GET['id']);

}

$category = Entity\Category::find($id);

$data = $category->getProducts();

$cat_name=$category->getName();
	$maxPerPage = 9;
	$currentPage = 1;
if (isset($_GET['page'])){
    $currentPage = $_GET['page'];
}
	$nrOfPages = ceil(count($data)/$maxPerPage);
	$startIndex = ($currentPage-1)*$maxPerPage;
	$data = array_slice($data, $startIndex, $maxPerPage);
//var_dump($data);die;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes">
    <title>WEB-04 ONLINE-SHOP</title>
    <link rel="stylesheet" type="text/css" href="stile.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://kit.fontawesome.com/ade0e905b9.js" crossorigin="anonymous"></script>
</head>
<body>
<?php include 'topbar.php';?>
<div id="content" class="container "  >
        <div id="menu" class="col-12 row menu ">
         <?php include 'header.php'; ?>
	    </div>

<div id="body">

    <div class="row" id="sidebar">
        <div id="main_content" class="col-2 localContent">
            <?php include 'sidebar.php'; ?>
        </div>

        <div id="fullcolor" class="col-7">
            <center><h2><?php echo $cat_name; ?></h2></center>


            <div class="row justify-content-center align-items-center">

                <?php foreach ($data as $product):
                    $card_title = substr($product->title, 0, 20);
                    $card_text = substr($product->description, 0, 40);
                    ?>
                    <div class='card' style='margin: 5px 5px; width: 200px;height: 450px'>
                        <img src='img/<?php echo "$product->photo"; ?>' class='card-img-top' style='height:155px;'>
                        <div class='card-body'>
                            <h5 class='card-title' style='color:blue'><a href="product.php?id=<?php echo intval($product->id); ?>"><?php echo "$card_title...";?></a></h5>
                            <p class='card-text'><?php echo "$card_text ...";?></p>
                            <a href="product.php?id=<?php echo intval($product->id); ?>" class="btn btn-outline-info">View</a>
                            <a href='?id=<?php echo $id; ?>&action=wishlist&code=<?php echo intval($product->id); ?>' class='btn btn-outline-danger'><i class='far fa-heart'></i></a>
                            <a href='?id=<?php echo $id; ?>&code=<?php echo intval($product->id); ?>' class='btn btn-warning' style='margin: 5px 5px; color: white;'><i class='fas fa-cart-plus' style=''> Buy</i></a>
                        </div>
                    </div>

                <?php endforeach; ?>
                <div>
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        if ($currentPage > 1) {
                            echo "
			<li class='page-item'><a class='page-link' href='category.php?page=1&id=$id'>&laquo;</a></li>
			<li class='page-item'> <a class='page-link' href='category.php?page=";
                            echo $currentPage - 1;
                            echo "&id=$id'>Previous</a></li>";
                        }
                        echo "<li class='page-item'><a class='page-link' href='#'>$currentPage</a></li>";
                        if ($currentPage < $nrOfPages) {
                            echo "
				<li class='page-item'><a class='page-link' href='category.php?page=";
                            echo $currentPage + 1;
                            echo "&id=$id'>Next</a></li>
				<li class='page-item'><a class='page-link' href='category.php?page=";
                            echo $nrOfPages;
                            echo "&id=$id'>&raquo;</a></li>";
                        }
                        ?>
                    </ul>
                </nav>
                </div>
            </div>

        </div>
        <div id="bestseller" class="col-3 ">
            <?php include 'bestseller.php'; ?>

        </div>
    </div>
</div>
<div id="footer"><?php include'footer.php';?></div>
</div>
</body>
</html>